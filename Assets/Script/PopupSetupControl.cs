﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupSetupControl : MonoBehaviour
{
    public GameObject mPageShortcut;
    public GameObject mPageVideo;

    private void Start()
    {
        OnClickTabShortcut();
    }

    private void OnEnable()
    {
        OnClickTabShortcut();
    }

    public void OnClickTabShortcut()
    {
        mPageShortcut.SetActive(true);
        mPageVideo.SetActive(false);
    }

    public void OnClickTabVideo()
    {
        mPageShortcut.SetActive(false);
        mPageVideo.SetActive(true);
    }
}
