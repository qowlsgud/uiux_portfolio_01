﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class IngameMenuControl : MonoBehaviour
{
    public GameObject mPopupScoreBoard;
    public void OnClickPopupScoreBoardOpen() { mPopupScoreBoard.SetActive(true); }
    public void OnClickPopupScoreBoardClose() { mPopupScoreBoard.SetActive(false); }

    public GameObject mPopupSetup;
    public void OnClickPopupSetupOpen() { mPopupSetup.SetActive(true); }
    public void OnClickPopupSetupClose() { mPopupSetup.SetActive(false); }

    public void OnClickGoLobby() { SceneManager.LoadScene("Scene_Lobby"); }

    private void Awake()
    {
        mPopupScoreBoard.SetActive(false);
        mPopupSetup.SetActive(false);
    }


}
