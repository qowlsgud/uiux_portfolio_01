﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LobbyMenuControl : MonoBehaviour
{
    private void Awake()
    {
        // 페이지 오브젝트들 세팅
        mPagesGOArr = new GameObject[] { mPage_Home_GO, mPage_Play_GO, mPage_Heroes_GO, mPage_Shop_GO };

        // 팝업메뉴 오브젝트를 세팅.
        mPopupMenuGOArr = new GameObject[] { mPopupMenu_MailList_GO, mPopupMenu_FriendList_GO, mPopupMenu_Profile_GO };

        // 팝업 오브젝트들 세팅
        mPopupGOArr = new GameObject[] { mPopup_Event_GO, mPopup_Mail_GO, mPopup_Setup_GO, mPopup_AddFriend_GO };

        // 빈공간 버튼 세팅
        mEmptyAreaCloseBtn.SetActive(false);

        // 채팅 오브젝트들 세팅
        mPopupMenu_Chatting_GO.SetActive(false);
        mPopupMenu_ChattingInfo_GO.SetActive(false);

        // 홈 화면으로 해두기
        PageToggle(0);

        // 이벤트 팝업을 띄워놓기
        PopupToggle(1);

        // 모든 팝업메뉴 닫기
        PopupMenuAllClose();
    }

    // 플레이버튼
    public void OnClickGamePlay()
    {
        SceneManager.LoadScene("Scene_InGame");
    }


    // page 오브젝트 토글 //////////////////////////////////////////////////////////////////////////////////////////////////
    public GameObject mPage_Home_GO;
    public GameObject mPage_Play_GO;
    public GameObject mPage_Heroes_GO;
    public GameObject mPage_Shop_GO;

    private GameObject[] mPagesGOArr;

    public void OnClickHomePage() { PageToggle(0); }
    public void OnClickPlayPage() { PageToggle(1); }
    public void OnClickHeroesPage() { PageToggle(2); }
    public void OnClickShopPage() { PageToggle(3); }

    private void PageToggle(int _idx)
    {
        int len = mPagesGOArr.Length;
        for (int i = 0; i < len; i++)
        {
            if (i == _idx) mPagesGOArr[i].SetActive(true);
            else mPagesGOArr[i].SetActive(false);
        }

        PopupMenuAllClose();
        ChattingAllClose();
        mEmptyAreaCloseBtn.SetActive(false);
    }


    // popup 오브젝트 토글 ////////////////////////////////////////////////////////////////////////////////////////////
    public GameObject mPopup_Event_GO;
    public GameObject mPopup_Mail_GO;
    public GameObject mPopup_Setup_GO;
    public GameObject mPopup_AddFriend_GO;

    private GameObject[] mPopupGOArr;
    private bool mIsPopupOpened = false;

    public void OnClickEventPopup() { PopupToggle(0); }
    public void OnClickMailPopup() { PopupToggle(1); }
    public void OnClickSetupPopup() { PopupToggle(2); }
    public void OnCllickAddFriendPopup() { PopupToggle(3); }

    private void PopupToggle(int _idx)
    {
        int len = mPopupGOArr.Length;
        if (!mIsPopupOpened)
        {
            mIsPopupOpened = true;

            for (int i = 0; i < len; i++)
            {
                if (i == _idx) mPopupGOArr[i].SetActive(true);
                else mPopupGOArr[i].SetActive(false);
            }
        }

        else
        {
            mIsPopupOpened = false;

            for (int i = 0; i < len; i++)
            {
                mPopupGOArr[i].SetActive(false);
            }
        }

        PopupMenuAllClose();
        ChattingAllClose();
        mEmptyAreaCloseBtn.SetActive(false);
    }


    // popup menu와 chatting을 모두 close함.////////////////////////////////////////////////////////////////////////////
    public GameObject mEmptyAreaCloseBtn;
    public void OnClickEmptyAreaClose()
    {
        PopupMenuAllClose();
        ChattingAllClose();
        mEmptyAreaCloseBtn.SetActive(false);
    }

    // popup menu 오브젝트 토글 //////////////////////////////////////////////////////////////////////////////////////////////
    public GameObject mPopupMenu_MailList_GO;
    public GameObject mPopupMenu_FriendList_GO;
    public GameObject mPopupMenu_Profile_GO;

    private GameObject[] mPopupMenuGOArr;

    public void OnClickMailList() { PopupMenuToggle(0); mEmptyAreaCloseBtn.SetActive(true); }
    public void OnClickFriendList() { PopupMenuToggle(1); mEmptyAreaCloseBtn.SetActive(true); }
    public void OnClickProfile() { PopupMenuToggle(2); mEmptyAreaCloseBtn.SetActive(true); }
    
    private void PopupMenuToggle(int _idx)
    {
        int len = mPopupMenuGOArr.Length;
        for (int i = 0; i < len; i++)
        {
            if (i == _idx) mPopupMenuGOArr[i].SetActive(true);
            else mPopupMenuGOArr[i].SetActive(false);
        }

        ChattingAllClose();
    }

    private void PopupMenuAllClose()
    {
        int len = mPopupMenuGOArr.Length;
        for (int i = 0; i < len; i++)
        {
            mPopupMenuGOArr[i].SetActive(false);
        }
    }


    //채팅/////////////////////////////////////////////////////////////////////////////
    public GameObject mPopupMenu_Chatting_GO;
    public GameObject mPopupMenu_ChattingInfo_GO;

    public void OnSelectChattingInput()
    {
        PopupMenuAllClose();
        mPopupMenu_Chatting_GO.SetActive(true);
        mEmptyAreaCloseBtn.SetActive(true);
    }

    
    public void OnClickChattingInfoClick()
    {
        PopupMenuAllClose();
        mEmptyAreaCloseBtn.SetActive(true);

        if (mPopupMenu_Chatting_GO.activeSelf)
        {
            mPopupMenu_ChattingInfo_GO.SetActive(!mPopupMenu_ChattingInfo_GO.activeSelf);
        }
        else
        {
            if(!mPopupMenu_ChattingInfo_GO.activeSelf)
            {
                mPopupMenu_Chatting_GO.SetActive(true);
                mPopupMenu_ChattingInfo_GO.SetActive(true);
            }
        }
    }

    private void ChattingAllClose()
    {
        mPopupMenu_Chatting_GO.SetActive(false);
        mPopupMenu_ChattingInfo_GO.SetActive(false);
    }
   
}
